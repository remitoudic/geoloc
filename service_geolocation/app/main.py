from fastapi import Depends, FastAPI
from sqlalchemy.future import select
from sqlalchemy.ext.asyncio import AsyncSession
import time 


from app.db import get_session, init_db
from app.models import Marker, MarkerCreate

from app.geoloc_logic.fake_data import addresses
from app.geoloc_logic.controller import *
import ptvsd
import requests
from fastapi.middleware.cors import CORSMiddleware
from app.api import ping, marker
import json 


app = FastAPI()

app.include_router(ping.router)
app.include_router(marker.router)
#ptvsd.enable_attach(address=('0.0.0.0', 8001))

@app.on_event("startup")
async def on_startup():
    await init_db()


@app.post("/locationIQ", tags=["service"])
async def add_marker(marker: MarkerCreate, session: AsyncSession = Depends(get_session)):
    
    # call  service
    raw_data = Geocoder.geocode(marker.address)

    # save to database 
    marker = Marker(name=marker.name, 
                    address= marker.address,
                    geolocated_address= json.dumps(raw_data[0])) 


    session.add(marker)
    await session.commit()
    await session.refresh(marker)
    return marker


@app.get("/return_geolocationIQ_posts", response_model=list[Marker],  tags=["service"],)
async def get_markers(session: AsyncSession = Depends(get_session)):
    result = await session.execute(select(Marker))
    markers = result.scalars().all()

    formated_address=json.loads(markers[0].geolocated_address)
    print(formated_address)
    
    return [Marker(name=marker.name, 
                   address=marker.address, 
                   geolocated_address=marker.geolocated_address,
                   id=marker.id) 
                   for marker in markers]