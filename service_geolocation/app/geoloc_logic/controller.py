

# from test_data import addresses
# from geocode import Geocoder
# import time

from .fake_data import addresses
from .geocode import Geocoder
import time


def call_API():
    data=[]
    C = 0
    for address_data in addresses:
        C = C + 1
        print("############")
        print(C ," - Search for:", address_data["address"])
        result=Geocoder.geocode( address_data["address"] )
        data.append(result)
        time.sleep(1)
    return data
    