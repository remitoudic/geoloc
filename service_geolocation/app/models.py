from sqlmodel import SQLModel, Field
from sqlalchemy import Column
from sqlalchemy.dialects.postgresql import JSONB

class MarkerBase(SQLModel):
    name: str
    address: str
    geolocated_address: str
    #geolocated_address: Field(sa_column=Column(JSONB))


class Marker(MarkerBase, table=True):
    id: int = Field(default=None, primary_key=True)


class MarkerCreate(MarkerBase):
    pass