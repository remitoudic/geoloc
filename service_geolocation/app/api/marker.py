from fastapi import APIRouter,Depends
from app.models import Marker, MarkerCreate
from app.db import get_session, init_db
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select
router = APIRouter()


@router.get("/markers", response_model=list[Marker],  tags=["dev. purpose"],)
async def get_markers(session: AsyncSession = Depends(get_session)):
    result = await session.execute(select(Marker))
    markers = result.scalars().all()
    
    return [Marker(name=marker.name, 
                   address=marker.address, 
                   geolocated_address=marker.geolocated_address,
                   id=marker.id) 
                   for marker in markers]


@router.post("/markers", tags=["dev. purpose"])
async def add_marker(marker: MarkerCreate, session: AsyncSession = Depends(get_session),):
    marker = Marker(name=marker.name, 
                    address=marker.address,
                    geolocated_address= marker.geolocated_address )

    session.add(marker)
    await session.commit()
    await session.refresh(marker)
    return marker