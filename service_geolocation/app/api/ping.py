from fastapi import APIRouter

router = APIRouter()


@router.get("/ping", tags=["monitoring purpose"])
def health_check():
    return {"health_status": "OK", "service_geolocalisation": "service Z"}



