import pytest
from starlette.testclient import TestClient
import json
import time

from app.geoloc_logic.fake_data import addresses
from app.main import app

data_fake = {
    "name": "EinsZweiDrei",
    "address": "UnDeuxTrois",
    "geolocated_address": "UnDosTres",
}


@pytest.fixture(scope="module")
def test_app():
    client = TestClient(app)
    yield client


def test_create_marker():
    client = TestClient(app)

    response = client.post(
        "/markers",
        headers={"accept": "application/json", "Content-Type": "application/json"},
        data=json.dumps(data_fake),
    )

    # first check
    assert response.status_code == 200

    # # snd check
    response = response.json()
    del response["id"]
    assert response == data_fake


def test_read_markers():

    client = TestClient(app)
    response = client.get("/markers")

    # first check
    assert response.status_code == 200

    # snd check
    assert len(response.json()) >= 1


def test_create_geoloc_adress():

    payload = {
        "name": "EinsZweiDrei",
        "address": "World of books limited 4281 express ln suite 13647 Sarasota Florida 34238 US",
        "geolocated_address": "",
    }
    client = TestClient(app)

    response = client.post(
        "/locationIQ",
        headers={"accept": "application/json", "Content-Type": "application/json"},
        data=json.dumps(payload),
    )
    # first check
    assert response.status_code == 200


def test_post_all_the_adress():

    for elt in addresses:
        payload = {
            "name": "EinsZweiDrei",
            "address": elt["address"],
            "geolocated_address": "",
        }
        client = TestClient(app)

        response = client.post(
            "/locationIQ",
            headers={"accept": "application/json", "Content-Type": "application/json"},
            data=json.dumps(payload),
        )

        # first check
        assert response.status_code == 200
        time.sleep(1)

# deactivated because it takes 15 seconds at least .... 

# def test_post_bad_adresss():

#     payload = {
#         "name": "EinsZweiDrei",
#         "address": {"some_json": "alibaba"},
#         "geolocated_address": "",
#     }
#     client = TestClient(app)

#     response = client.post(
#         "/locationIQ",
#         headers={"accept": "application/json", "Content-Type": "application/json"},
#         data=json.dumps(payload),
#     )

#     # first check
#     assert response.status_code != 200
