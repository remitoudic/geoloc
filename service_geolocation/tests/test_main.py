from starlette.testclient import TestClient
from app.main import app
from tests.conftest import *
from app.geoloc_logic.geocode import Geocoder

# Standard library imports...
from unittest.mock import Mock, patch

# heath check 
def test_ping(test_app):
    response = test_app.get("/ping")
    assert response.status_code == 200
    assert response.json() == {
        "health_status": "OK",
        "service_geolocalisation": "service Z",
    }


# fake = eval("""[{'address': {'company': 'World Of Books Ltd', 'building': 'Suite 13647', 'number': '4281', 'estate': '', 'street': 'Express Lane', 'village_suburb': None, 'town_city': 'Sarasota', 'region': 'Florida', 'postal_code': '34238', 'country': 'United States', 'country_code': 'US'}, 'formatted_address': 'World Of Books Ltd, Suite 13647, 4281, Express Lane, Sarasota, 34238, United States', 'osm': {'details': {'distance': None, 'osm_type': 'way', 'osm_id': '636892231', 'bounding_box': ['27.2674522', '27.2676393', '-82.4826308', '-82.4817941'], 'latitude': '27.2674989', 'longitude': '-82.4821877', 'display_name': 'Express Lane, Bee Ridge, Sarasota County, Florida, 34238, USA', 'class': 'highway', 'type': 'residential', 'importance': 0.4}, 'address': {'house_number': None, 'road': 'Express Lane', 'residential': None, 'borough': None, 'neighbourhood': None, 'quarter': None, 'hamlet': None, 'suburb': None, 'island': None, 'village': None, 'town': None, 'city': 'Bee Ridge', 'city_district': None, 'county': 'Sarasota County', 'state': 'Florida', 'state_district': None, 'postcode': '34238', 'country': 'United States of America', 'country_code': 'us', 'state_code': None}}, 'validation': {'distance': inf, 'address_match_level': 'road'}}]""")
    
# fake = fake[0][0]

# # humm little test about the backend logic ....  
# def test_getting_a_geolocated_adress():
#     # Configure the mock to return a response with an OK status code.

#     # Call the service to hit the mocked API.
#     with patch("app.geoloc_logic.geocode.Geocoder") as mock_get:
#         mock_get.return_value = fake

#     # Call the service, which will send a request to the server.
#     response = Geocoder.geocode()
#     response = json.dumps(response)
#     response = eval(response)
#     reponse_dict = response[0]

#     # If the request is sent successfully, then I expect a response to be returned.
#     assert reponse_dict == fake