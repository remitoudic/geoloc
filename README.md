# Micro service initial setting

Code related to Jira'S ticket 7044 


## How to start the project ?


- Install docker compose (version ≥ 2)
- put  the location folder_files folder in  /geoloc/service_geolocation/app/geoloc_logic
- run docker compose up --build  at the root level (geoloc)


## Microservice  schema 
<img src="ms_schema.png" width="200">


## Microservice  Swagger docu 
Test out the following routes:

1. PROXY : [http://localhost:80/docs](http://localhost:80/docs)
3. Service:   Geolocalisation [http://localhost:8004/docs](http://localhost:8004/docs)


## Feature you can try :

    For any service a swagger documentation is available at the  <runningport_of_the servive>/docs 

    Authentification:   The authentification is made at the level of the gateway via token and password.  One fake user is set with username  "johndoe" and passord "secret".

    Proxy:  After authenticating with the fake user you can test the routing to another service. 
    There is an endpoint to access  "service_z" (healthcare endpoint).

    Server with get and post method:  cf swagger

    


## Working with docker

    The "docker-compose" file is used to structure and  build the docker images to work togehter.  The creation of the images and of the network is make by the commmand "docker build".  You need to do it once and if you code change  you have have to rebuild.  
    After  building  your image you need to start them . 

    ==> docker compose  up --build 

    the command (re)builds the images (with any significant changes) you  made and start them. 

    To stopt  the the all projects  needs to stops all images. 

    ==> docker compose down -v 


## Run unit test 

   
    -   (Optionnal) first the docker  compose TEST  
            docker-compose  -f docker-compose_TEST.yaml up --build
    -   Pytest inside the container 
            docker-compose exec service_geolocation  pytest .

## debug a service 
    
     docker  run -i -t <service name> sh   






    

    

