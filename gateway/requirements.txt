ptvsd
debugpy

fastapi>=0.68.0,<0.69.0
uvicorn>=0.15.0,<0.16.0

python-jose[cryptography]
passlib[bcrypt]
python-multipart

aiohttp