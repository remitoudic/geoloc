from fastapi import APIRouter

router = APIRouter()




@router.get("/health_check", tags=["Gateway"])
def health_check():
    return {"health_status": "OK", "service_name": "API Gateway"}




