from pydantic import BaseSettings


class Settings(BaseSettings):
    geoloc_url: str = "http://172.17.0.1"
    geoloc_port: str = "4000"
    geoloc2_url: str = "http://localhost"
    geoloc2_port: str = "5020"


settings = Settings()
