import ptvsd
from fastapi import FastAPI
import aiohttp

from .config import Settings, settings

from .routers import proxy

# for login / auth
from .auth.auth import *
from .models import *
from .auth.fake_users_db import *
from fastapi.security import  OAuth2PasswordRequestForm




ptvsd.enable_attach(address=('0.0.0.0', 81))

app = FastAPI(title="API Gateway")
app.include_router(proxy.router)


@app.get("/users", tags=["Auth"], )
async def read_users( token: str = Depends(oauth2_scheme)):
    return [{"username": "johndoe"}, {"username": "Morty"}]


@app.post("/token", response_model=Token,  tags=["Auth"])
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    """
    login endpoint for access_token , input: username, pwd
    """
    user = authenticate_user(fake_users_db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"user_name": user.username}, expires_delta=access_token_expires
    )
    return Token(access_token= access_token, token_type= "Bearer")


@app.get("/",  tags=["Gateway"])
def root():
    print  ( "debugger rununing")
    # to test the debugger 
    var= { "remi": {"name":{"name": "toudic5"}}}
    return {"Gateway": "root"}


@app.get("/gateaway_service_geoloc", tags=["proxy"])
#async def proxy(url:str=f"{settings.geoloc_url}:{settings.geoloc_port}/health_check",  
# async def proxy(url:str="http://0.0.0.0:8004/health_check",  

#               
@app.get("/geolocalisation_health_check", tags=["proxy"])
async def proxy(url:str="http://192.168.224.1:8004/ping",token: str = Depends(oauth2_scheme)):
    """ redirects to endpoint / services"""

    async with aiohttp.ClientSession() as session:
            async with session.get(url) as response:
                data = await response.json()
                return (data, response.status)

